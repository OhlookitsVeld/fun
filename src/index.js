'use strict';

const path = require('path');

class Fun {
	constructor(base, api) {
		this.base = base;
		this.api = api;

		const { version } = require('../package.json');
		this.version = version;

		this.public = null;
	}

	async onMount() {
		this.register = await this.api.Commands.loadCommands(path.resolve(__dirname, 'Commands'));
	}

	async onUnmount() {
		await this.public.unregister();
	}
}

module.exports = Fun;
