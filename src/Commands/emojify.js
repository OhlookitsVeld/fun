'use strict';

const axios = require('axios');

class Emojify {
	constructor() {
		this.name = 'emojify';
	}

	async execute({ channel, input }) {
		const args = input.args.join(' ');
		if (!args) return channel.sendMessage('Correct usage: `=emojify <words>`');
		const request = await axios.get(`https://emoji.getdango.com/api/emoji?q=${encodeURIComponent(args)}`);
		if (!request.data || !request.data.results) return channel.sendMessage(`Sorry, upstream error!`);

		const emojified = request.data.results.reduce((a, emoji) => `${a}${emoji.text}`, '');

		return channel.sendMessage('FUN_EMOJIFY_RESULT', { name: args, emojified });
	}
}

module.exports = Emojify;
