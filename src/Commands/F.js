'use strict';

const path = require('path');

class F {
	constructor(base) {
		this.base = base;

		this.name = 'f';
		this.alias = ['respect', 'respects', 'rip'];

		this.keyTotal = 'respects.total';
		this.keyToday = 'respects.today';
	}

	async getCounts() {
		const Redis = await this.base.getPrimary('Redis');
		const today = await Redis.get(this.keyToday) || 0;
		const total = await Redis.get(this.keyTotal) || 0;

		return { today, total };
	}

	async execute({ input, channel, author, translator }) {
		const Redis = await this.base.getPrimary('Redis');
		if (await input.get({ type: 'opt', name: 'stats', validator: 'none', optional: true })) {
			const { today, total } = await this.getCounts();

			return translator.sendMessage('FUN_RESPECTS_STATS', { numToday: today, numAll: total });
		}

		// incr counters
		await Redis.incr(this.keyTotal);
		await Redis.incr(this.keyToday);

		// handle daily ttl stuff
		const ttl = await Redis.ttl(this.keyToday);
		if (ttl < 0) await Redis.expire(this.keyToday, 86400);

		// TODO: Fix anti-pattern direct access to InputParser.args, ie { type: 'arg', start: 0, count: -1 }
		const args = input.args.join(' ');
		let description = null;
		if (args) description = translator.get('FUN_PAID_RESPECTS_FOR', { username: author.username, for: args });
		else description = translator.get('FUN_PAID_RESPECTS', { username: author.username });

		const { today, total } = await this.getCounts();
		return channel.sendMessage('', { zws: false }, {
			description,
			footer: {
				text: translator.get('FUN_RESPECTS_FOOTER', { numToday: today, numAll: total }),
			},
		});
	}
}

module.exports = F;
