'use strict';

const axios = require('axios');

class Coffee {
	constructor() {
		this.name = 'coffee';
	}

	async execute({ channel }) {
		axios('https://coffee.alexflipnote.xyz/random.json').then(resp => {
			channel.sendMessage('', { zws: false }, {
				title: 'Coffee ☕', // TODO: translation api?
				image: {
					url: resp.data.file,
				},
			});
		}).catch(error => {
			// how to error handling?
		});
	}
}

module.exports = Coffee;
