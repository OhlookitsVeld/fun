'use strict';

class Tableflip {
	constructor() {
		this.name = 'tableflip';
		this.flips = [
			' (╯°□°）╯︵ ┻━┻',
			'(┛◉Д◉)┛彡┻━┻',
			'(ﾉ≧∇≦)ﾉ ﾐ ┸━┸',
			'(ノಠ益ಠ)ノ彡┻━┻',
			'(╯ರ ~ ರ）╯︵ ┻━┻',
			'(┛ಸ_ಸ)┛彡┻━┻',
			'(ﾉ´･ω･)ﾉ ﾐ ┸━┸',
			'(ノಥ,_｣ಥ)ノ彡┻━┻',
			'(┛✧Д✧))┛彡┻━┻',
		];
	}

	async execute({ channel }) {
		channel.sendMessage(this.flips[Math.floor(Math.random() * this.flips.length)]);
	}
}

module.exports = Tableflip;

